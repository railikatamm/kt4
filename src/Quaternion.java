import java.util.*;

/**
 * Quaternions. Basic operations.
 */
public class Quaternion {

    private final double a, b, c, d;
    private static double threshold = 0.000000001;

    /**
     * Constructor from four double values.
     *
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    /**
     * Real part of the quaternion.
     *
     * @return real part
     */
    public double getRpart() {
        return a;
    }

    /**
     * Imaginary part i of the quaternion.
     *
     * @return imaginary part i
     */
    public double getIpart() {
        return b;
    }

    /**
     * Imaginary part j of the quaternion.
     *
     * @return imaginary part j
     */
    public double getJpart() {
        return c;
    }

    /**
     * Imaginary part k of the quaternion.
     *
     * @return imaginary part k
     */
    public double getKpart() {
        return d;
    }

    /**
     * Conversion of the quaternion to the string.
     *
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {
        String asString = "";

        if (!(Math.abs(0 - a) < threshold)) {
            asString = String.valueOf(a);
        }

        if (!(Math.abs(0 - b) < threshold)) {
            if (b < threshold) {
                asString += String.valueOf(b);
            } else {
                asString += "+" + String.valueOf(b);
            }
            asString += "i";
        }

        if (!(Math.abs(0 - c) < threshold)) {
            if (c < threshold) {
                asString += String.valueOf(c);
            } else {
                asString += "+" + String.valueOf(c);
            }
            asString += "j";
        }

        if (!(Math.abs(0 - d) < threshold)) {
            if (d < threshold) {
                asString += String.valueOf(d);
            } else {
                asString += "+" + String.valueOf(d);
            }
            asString += "k";
        }

        return asString;
    }

    /**
     * Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     *
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     * @throws IllegalArgumentException if string s does not represent
     *                                  a quaternion (defined by the <code>toString</code> method)
     */
    public static Quaternion valueOf(String s) {
        if (s.indexOf('j') < s.indexOf('i') && s.indexOf('j') != -1 || s.indexOf('k') < s.indexOf('i') && s.indexOf('k') != -1 || s.indexOf('k') < s.indexOf('j') && s.indexOf('k') != -1) {
            throw new RuntimeException("Values in " + s + " given in incorrect order for Quaternion");
        }

        double a = 0;
        double b = 0;
        double c = 0;
        double d = 0;

        if (s.equals("")){
            return new Quaternion(a, b, c, d);
        }

        ArrayList<String> values = new ArrayList();

        boolean negative = false;
        String copy = s;

        while (true) {
            negative = false;
            if (copy.charAt(0) == '-') {
                negative = true;
                copy = copy.substring(1);
            }

            int plusIndex = copy.indexOf("+");
            int minusIndex = copy.indexOf("-");

            if (plusIndex == minusIndex) {
                if (negative){
                    copy = "-" + copy;
                }
                values.add(copy);
                break;
            }

            if (minusIndex == -1) {
                minusIndex = copy.length();
            }
            if (plusIndex == -1) {
                plusIndex = copy.length();
            }

            if (plusIndex < minusIndex){
                String element = copy.substring(0, plusIndex);
                if (negative) {
                    element = "-" + element;
                }
                values.add(element);
                copy = copy.substring(plusIndex + 1);
            } else if (minusIndex < plusIndex){
                String element = copy.substring(0, minusIndex);
                if (negative) {
                    element = "-" + element;
                }
                values.add(element);
                copy = copy.substring(minusIndex);
            }
        }

        for (String value : values) {
            if (value.contains("i") || value.contains("j") || value.contains("k")){
                double parseDouble = Double.parseDouble(value.substring(0, value.length() - 1));
                if (value.charAt(value.length() - 1) == 'i' && (Math.abs(0 - b) < threshold)) {
                    b = parseDouble;
                } else if (value.charAt(value.length() - 1) == 'j' && (Math.abs(0 - c) < threshold)) {
                    c = parseDouble;
                } else if (value.charAt(value.length() - 1) == 'k' && (Math.abs(0 - d) < threshold)) {
                    d = parseDouble;
                } else {
                    throw new RuntimeException("Tried to assign multiple values to same quaternion parameter");
                }
            } else {
                if ((Math.abs(0 - a) < threshold)) {
                    a = Double.parseDouble(value);
                } else {
                    throw new RuntimeException("Tried to assign multiple values to same quaternion parameter");
                }
            }
        }

        return new Quaternion(a, b, c, d);
    }

    /**
     * Clone of the quaternion.
     *
     * @return independent clone of <code>this</code>
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Quaternion(this.a, this.b, this.c, this.d);
    }

    /**
     * Test whether the quaternion is zero.
     *
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {
        return Math.abs(0 - a) < threshold && Math.abs(0 - b) < threshold && Math.abs(0 - c) < threshold && Math.abs(0 - d) < threshold;
    }

    /**
     * Conjugate of the quaternion. Expressed by the formula
     * conjugate(a+bi+cj+dk) = a-bi-cj-dk
     *
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {
        return new Quaternion(a, -b, -c, -d);
    }

    /**
     * Opposite of the quaternion. Expressed by the formula
     * opposite(a+bi+cj+dk) = -a-bi-cj-dk
     *
     * @return quaternion <code>-this</code>
     */
    public Quaternion opposite() {
        return new Quaternion(-a, -b, -c, -d);
    }

    /**
     * Sum of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     *
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus(Quaternion q) {
        return new Quaternion(a + q.a, b + q.b, c + q.c, d + q.d);
    }

    /**
     * Product of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     *
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times(Quaternion q) {

        double r = a * q.a - b * q.b - c * q.c - d * q.d;
        double i = a * q.b + b * q.a + c * q.d - d * q.c;
        double j = a * q.c - b * q.d + c * q.a + d * q.b;
        double k = a * q.d + b * q.c - c * q.b + d * q.a;

        return new Quaternion(r, i, j, k);
    }

    /**
     * Multiplication by a coefficient.
     *
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times(double r) {
        return new Quaternion(a * r, b * r, c * r, d * r);
    }

    /**
     * Inverse of the quaternion. Expressed by the formula
     * 1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     * ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
     *
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        if (isZero()) {
            throw new RuntimeException("Cannot inverse a quaternion that is zero");
        }
        double inv = a * a + b * b + c * c + d * d;
        return new Quaternion(a / inv, -b / inv, -c / inv, -d / inv);
    }

    /**
     * Difference of quaternions. Expressed as addition to the opposite.
     *
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus(Quaternion q) {
        return new Quaternion(a - q.a, b - q.b, c - q.c, d - q.d);
    }

    /**
     * Right quotient of quaternions. Expressed as multiplication to the inverse.
     *
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException("Cannot divide by right with a quaternion that is zero");
        }
        return times(q.inverse());
    }

    /**
     * Left quotient of quaternions.
     *
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException("Cannot divide by left with a quaternion that is zero");
        }
        Quaternion a = this;
        return q.inverse().times(a);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Quaternion)) return false;

        Quaternion that = (Quaternion) o;
        return Math.abs(that.a - this.a) < threshold && Math.abs(that.b - this.b) < threshold && Math.abs(that.c - this.c) < threshold && Math.abs(that.d - this.d) < threshold;
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b, c, d);
    }

    /**
     * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     *
     * @param q factor
     * @return dot product of this and q
     */
    public Quaternion dotMult(Quaternion q) {
        return (times(q.conjugate()).plus(q.times(conjugate()))).times(0.5);
    }

    /**
     * Norm of the quaternion. Expressed by the formula
     * norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     *
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        return Math.sqrt(a * a + b * b + c * c + d * d);
    }

    /**
     * Main method for testing purposes.
     *
     * @param arg command line parameters
     */
    public static void main(String[] arg) {
        Quaternion q = new Quaternion(0, 0, 0, 0);
        System.out.println(q.toString());
    }
}
// end of file
